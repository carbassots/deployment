DOCKER_TAG := latest
REGISTRY_HOST := registry.gitlab.com
USERNAME := carbassots
NAME := deployment

all: pull run

all-silence: pull run-silence

generate: build push

pull:
	docker-compose pull

build:
	docker build -t $(REGISTRY_HOST)/$(USERNAME)/$(NAME):$(DOCKER_TAG) .

push:
	docker push $(REGISTRY_HOST)/$(USERNAME)/$(NAME):$(DOCKER_TAG)

run:
	docker-compose up

run-silence:
	docker-compose up -d
